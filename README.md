# Laboratoire #3 -- Solution

## Contenu

Le dépôt contient les fichier suivants.

```
.
├── arrays.c
├── Makefile
├── palindromes.c
├── strings.c
```

Je vous invite à regarder le contenu du fichier `Makefile` afin de vous
familiariser encore avec sa syntaxe. Remarquez les appels à `make` qui sont
suggérés et comparez avec les mots clefs présents dans le fichier.

## `strings.c`

### Compilation

```sh
$ make strings
```

### Tests

Faites `./strings toto titi tata`. Observez la sortie et le code.

## `palindromes.c`

### Compilation

```sh
$ make palindromes
```

### Tests

Les appels suivants sont faits pour fins de test:

```c
is_palindrome("radar")
is_palindrome("ici")
is_palindrome("laval")
is_palindrome("!RessasseR!")
is_palindrome("")
is_palindrome("U")
```

La sortie attendue peut ressembler à cela:

```
Est-ce que "radar" est un palindrome ? oui
```

Testez-le vous-même! **Implémentez la fonction manquante**
`estPhrasePalindrome`.

## `arrays.c`

### Compilation

```sh
$ make arrays
```

### Tests

Exécutez le programme avec `./arrays`.

## Auteur

Simon Désaulniers (sim.desaulniers@gmail.com)

