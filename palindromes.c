//----------------------------------------------------------
// File: palindromes.c          Author(s): Simon Désaulniers
// Date: 2018-01-30
//----------------------------------------------------------
// Laboratoire #3 du cours INF3135 à l'Université du
// Québec à Montréal à la session d'hiver 2018.
//----------------------------------------------------------

#include <stdio.h>
#include <string.h>
#include <stdbool.h>

bool is_palindrome(const char* candidate) {
    const size_t len = strlen(candidate);
    if (len <= 1)
        return true;
    size_t il = 0, ir = len-1;

    while (il < ir) {
        if (candidate[il] != candidate[ir])
            return false;
        ++il;
        --ir;
    }
    return true;
}

int main(int argc, char *argv[]) {
    printf("Est-ce que \"radar\" est un palindrome ? %s\n",
            is_palindrome("radar") ? "oui" : "non");
    printf("Est-ce que \"ici\" est un palindrome ? %s\n",
            is_palindrome("ici") ? "oui" : "non");
    printf("Est-ce que \"laval\" est un palindrome ? %s\n",
            is_palindrome("laval") ? "oui" : "non");
    printf("Est-ce que \"!RessasseR!\" est un palindrome ? %s\n",
            is_palindrome("!RessasseR!") ? "oui" : "non");
    printf("Est-ce que \"\" est un palindrome ? %s\n",
            is_palindrome("") ? "oui" : "non");
    printf("Est-ce que \"U\" est un palindrome ? %s\n",
            is_palindrome("U") ? "oui" : "non");
    return 0;
}

/* vim:set et sw=4 ts=4 tw=120: */

