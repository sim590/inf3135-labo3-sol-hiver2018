CC     := gcc
CFLAGS := $(if $(DEBUG),-g,) -c -Wall

CFILES   := strings.c palindromes.c array.c
BINARIES := strings palindromes arrays

all: strings palindromes arrays

arrays: arrays.o
	$(CC) -o $@ $<

palindromes: palindromes.o
	$(CC) -o $@ $<

strings: strings.o
	$(CC) -o $@ $<

%.o: %.c
	$(CC) $(CFLAGS) $<

clean:
	rm -f $(CFILES:.c=.o) $(BINARIES)

# vim:set noet sts=0 sw=4 ts=4 tw=120:

