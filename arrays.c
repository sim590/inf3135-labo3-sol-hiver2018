//----------------------------------------------------------
// File: arrays.c               Author(s): Simon Désaulniers
// Date: 2018-01-30
//----------------------------------------------------------
// Laboratoire #3 du cours INF3135 à l'Université du
// Québec à Montréal à la session d'hiver 2018.
//----------------------------------------------------------

#include <stdio.h>

const double* find_double(const double arr[], unsigned int len, double e) {
    size_t ofs = 0;
    while (ofs < len) {
        if (arr[ofs] == e)
            return arr+ofs;
        ++ofs;
    }
    return NULL;
}

void print_matrix(int matrix[3][3]) {
    for (size_t i = 0; i < 3; ++i) {
        for (size_t j = 0; j < 3; ++j) {
            printf("%d ", matrix[i][j]);
        }
        printf("\n");
    }
}

void sum_matrix(int lmat[3][3], int rmat[3][3], int smat[3][3]) {
    for (size_t i = 0; i < 3; ++i) {
        for (size_t j = 0; j < 3; ++j) {
            smat[i][j] = lmat[i][j] + rmat[i][j];
        }
    }
}

int main(int argc, char *argv[]) {
    double doubles[] = {1.0, 3.4, 5.6};
    const double* a = find_double(doubles, 3, 3.4);
    printf("L'adresse de 3.4 dans mon tableau est: %p\n", a);

    printf("Affichage de la matrice { {1, 2, 3}, {4, 5, 6}, {7, 8, 9} }:\n");
    int m[3][3] = { {1, 2, 3}, {4, 5, 6}, {7, 8, 9} };
    print_matrix(m);

    printf("La somme de la matrice\n");
    print_matrix(m);
    printf("avec elle-même est:\n");
    int rm[3][3] = {0};
    sum_matrix(m, m, rm);
    print_matrix(rm);
    return 0;
}

/* vim:set et sw=4 ts=4 tw=120: */

